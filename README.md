#QA-Sortable-Challenge

Two packages: 
- minderaapp (provided js application)
- minderatest (new module for qa-challenge with the solution for the given task [See](minderaapp/README.md)


##Test package

Technology: Java + JUnit framework

Build tool: Maven

## Containerizing the tests (Docker)

Dockerfiles can be found for each package

- minderaapp/Dockerfile
- minderatest/Dockerfile

######Steps:
In the project directory:

>1.  docker build -t mindera-app -f ./minderaapp/Dockerfile ./minderaapp
>2.  docker build -t mindera-test -f ./minderatest/Dockerfile ./minderatest
>3.  docker run -p 3000:3000 mindera-app:latest (from now on application is running on localhost:3000)
>4.  docker run mindera-test:latest (It will run the test(s)))

or use:
> docker-compose up --build 

