package selenium;

import PageObjects.AbstractPage;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * @author Balázs Ludrik
 */
public abstract class TestBase<P extends AbstractPage> {

    protected static WebDriver driver;

//    DesiredCapabilities capability = new DesiredCapabilities();

    private static String chromeDriverPath = "mac";
    protected P page;

    protected static final Logger log = Logger.getLogger(TestBase.class.getName());

    @BeforeClass
    public static void setUp() throws Exception{
        createDriver();
        driver.manage().window().fullscreen();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    private static WebDriver createDriver() throws Exception{
        ChromeOptions options = new ChromeOptions();
        String os = System.getProperty("os.name").toLowerCase();
        if (os.contains("mac")) {
            chromeDriverPath = "src/main/resources/mac/chromedriver";
        } else if(os.contains("linux") || os.contains("unix")) {
            chromeDriverPath = "/usr/bin/chromedriver";
            options.addArguments("--headless");
            options.addArguments("--no-sandbox");
        }

        System.setProperty("webdriver.chrome.driver", chromeDriverPath);
        log.info("Used chrome binary: " + chromeDriverPath);

        driver = new ChromeDriver(options);

        return driver;
    }

    @Before
    public void initPage() {
        this.page = createPage();
    }

    public abstract P createPage();

    @After
    public void cleanUp() {
        driver.manage().deleteAllCookies();
    }


    @AfterClass
    public static void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }


}
