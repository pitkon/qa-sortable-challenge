package selenium.tests;

import PageObjects.ListPage;
import org.junit.Test;
import selenium.TestBase;

/**
 * @author Balázs Ludrik
 */
public class SortingTest extends TestBase<ListPage> {

    @Override
    public ListPage createPage() {
        return new ListPage(this.driver);
    }

    @Test
    public void testDragAndDrop() {
        page.go();
        page.sortItemsInList();
        page.expect().isListSorted(true);
    }

}
