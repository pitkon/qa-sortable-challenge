package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

/**
 * @author Balázs Ludrik
 */
public class ListPage extends AbstractPage<ListPage>{

    public ListPage(WebDriver driver){
        super(driver);
    }

    private List<WebElement> getItemList() {
        List<WebElement> items = driver.findElements(By.cssSelector("#app ul li"));
        return items;
    }

    public void sortItemsInList() {
        List<WebElement> items = getItemList();
        for(int i=0;i<items.size();i++) {
            for(int j=0;j<items.size()-1;j++) {
                if(items.get(j).getText().compareToIgnoreCase(items.get(j+1).getText()) > 0) {
                    WebElement From = items.get(j+1);
                    WebElement To = items.get(j);

                    Actions action=new Actions(driver);
                    action.dragAndDrop(From, To).build().perform();
                }

            }
        }

        log.info("Sortation has finished.");
    }

    public ListPageExpect expect() {
        return new ListPageExpect(this);
    }

    public Boolean isListSorted() {
        List<WebElement> items = getItemList();
        for(int i=0; i<items.size()-1; i++) {
            if(items.get(i).getText().compareToIgnoreCase(items.get(i+1).getText()) > 0) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String getPath() {
        return "/index.html";
    }
}