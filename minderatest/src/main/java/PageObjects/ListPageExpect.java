package PageObjects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * @author Balázs Ludrik
 */
public class ListPageExpect {

    private final ListPage page;

    public ListPageExpect(ListPage page) {
        this.page = page;
    }

    public ListPageExpect isListSorted(Boolean sorted) {
        assertThat("List is sorted.", page.isListSorted(), is(sorted));
        return this;
    }
}
