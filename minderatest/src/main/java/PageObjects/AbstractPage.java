package PageObjects;

import org.openqa.selenium.JavascriptException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.logging.Logger;

/**
 * @author Balázs Ludrik
 */
public abstract class AbstractPage<P extends AbstractPage> {

    public final WebDriver driver;
    public WebDriverWait driverWait;
    protected static final String BASE_URL = "http://localhost:3000";
    public Logger log = Logger.getLogger(AbstractPage.class.getName());

    public AbstractPage(WebDriver driver) {
        this.driver = driver;
        this.driverWait = new WebDriverWait(driver, 5, 500);
    }

    public AbstractPage<P> go() {
        driver.get(BASE_URL+getPath());
        waitUntilPageIsLoaded();
        return this;
    }

    private void waitUntilPageIsLoaded() {
        driverWait.until( driver -> ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete") );
    }

    public abstract String getPath();


}
